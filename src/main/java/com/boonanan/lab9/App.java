package com.boonanan.lab9;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Scanner;

public class App {
    static Scanner sc = new Scanner(System.in);
    static Map map = new Map(15, 15);
    static Robot Robot = new Robot(map, 'A', 10, 10);
    static Tree Tree0 = new Tree(map, 5, 10);
    static Tree Tree1 = new Tree(map, 5, 6);
    static Tree Tree2 = new Tree(map, 6, 5);
    static Tree Tree3 = new Tree(map, 4, 8);
    static Tree Tree4 = new Tree(map, 9, 14);
    static Tree Tree5 = new Tree(map, 9, 9);
    static Tree Tree6 = new Tree(map, 1, 12);
    static Tree Tree7 = new Tree(map, 8, 10);
    static Tree Tree8 = new Tree(map, 7, 6);
    static Tree Tree9 = new Tree(map, 3, 8);

    public static String input() {
        return sc.next();
    }
    public static void process(String command) throws IOException, URISyntaxException {
        switch (command) {
            case "w":
                Robot.up();
                break;
            case "a":
                Robot.left();;
                break;
            case "s":
                Robot.down();;
                break;
            case "d":
                Robot.right();
                break;
            case "q":
                System.exit(0);
                break;
            case "wwssadadba":
                Desktop d = Desktop.getDesktop();
                d.browse(new URI("https://www.youtube.com/watch?v=dQw4w9WgXcQ"));
                System.exit(0);
                break;
            default:
                break;
        }
    }

    public static void main(String[] args) throws IOException, URISyntaxException {
        map.add(Robot);
        map.add(Tree0);
        map.add(Tree1);
        map.add(Tree2);
        map.add(Tree3);
        map.add(Tree4);
        map.add(Tree5);
        map.add(Tree6);
        map.add(Tree7);
        map.add(Tree8);
        map.add(Tree9);
        
        while (true) {
            map.print();
            String command = input();
            process(command);
        }

    }
}
